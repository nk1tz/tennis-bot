import * as fs from 'fs';
import * as path from 'path';

export type Surface = 'hard' | 'clay';

export interface BookingPreferences {
  email: string;
  password: string;
  playingDay: Date;
  hours: number[];
  surface: Surface;
  is2hours: boolean;
}

export const filePath = path.join(__dirname, 'userPreferences.json');

export const readPreferences = (): Partial<BookingPreferences> => {
  try {
    return require(filePath);
  } catch {
    return {};
  }
};

export const savePreferences = (preferences: Partial<BookingPreferences>) => {
  fs.writeFileSync(filePath, JSON.stringify(preferences, null, 2));
};

import { Page, Browser } from 'puppeteer';
import * as puppeteer from 'puppeteer';
import * as path from 'path';
import * as fs from 'fs';
import { flattenDeep, reverse } from 'lodash';
import { BookingPreferences, Surface } from './preferences';

export class StopError extends Error {}
export class InvalidCredentialError extends StopError {}

export const log = (...args: any[]) =>
  console.log(new Date().toISOString(), '-', ...args);

export const retry = async (
  /** 0 means infinite  */
  maxRetries: number,
  callback: () => any,
  onRetry = (error: any) => {
    console.log(`Error: ${error}`);
    console.log(`Retring...`);
    return true;
  },
) => {
  maxRetries = maxRetries || Number.MAX_SAFE_INTEGER;
  while (true) {
    try {
      return await callback();
    } catch (error) {
      maxRetries--;
      if (!maxRetries || !onRetry(error)) {
        throw error;
      }
    }
  }
};

export const withRetry = <F extends Function>(f: F): F =>
  ((...args: any[]) => retry(0, () => f(...args))) as any;

export const daysOfWeek = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

export const getNextBookableDay = () => {
  const date = new Date();
  date.setDate(date.getDate() + 3);
  return date.getDay();
};

export const waitUntil = (date: Date) => {
  const now = new Date();
  return new Promise(resolve =>
    setTimeout(resolve, date.getTime() - now.getTime()),
  );
};

export const wait = (ms: number) =>
  new Promise(resolve => setTimeout(resolve, ms));

export const dateAdd = (
  date: Date,
  { d, m, s }: { d?: number; m?: number; s?: number },
) => {
  const newDate = new Date(date);
  if (d) newDate.setDate(newDate.getDate() + d);
  if (m) newDate.setMinutes(newDate.getMinutes() + m);
  if (s) newDate.setSeconds(newDate.getSeconds() + s);
  return newDate;
};

const SurfaceIds = {
  clay: 18,
  hard: 16,
};

const openPage = async (browser: Browser, url: string) => {
  const page = await browser.newPage();
  await page.setViewport({ width: 1200, height: 700 });
  await retry(10, () => page.goto(url));
  return page;
};

export const withLoggedBrowser = async (
  prefs: BookingPreferences,
  callback: (browser: Browser) => Promise<any>,
) => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true,
    // slowMo: 200,
  });
  try {
    await login(prefs, browser);
    await callback(browser);
  } finally {
    browser.close();
  }
};

export const getCalendarUrl = (date: Date, surface: Surface) =>
  [
    `http://book.stadeiga.com/courtbooking/home/calendarDayView.do?`,
    `id=`,
    SurfaceIds[surface],
    `&iYear=`,
    date.getFullYear(),
    `&iMonth=`,
    date.getMonth(),
    `&iDate=`,
    date.getDate(),
  ].join('');

export const promiseFilter = async <T>(
  array: T[],
  iteratee: (item: T) => any,
): Promise<T[]> => {
  const results = await Promise.all(array.map(iteratee));
  return array.filter((item, i) => results[i]);
};

export const takeScreenshot = (page: Page, name: string) => {
  const screenShotDir = path.join(__dirname, `screenshots`);
  if (!fs.existsSync(screenShotDir)) {
    fs.mkdirSync(screenShotDir);
  }
  return page.screenshot({
    path: path.join(screenShotDir, `${new Date().toISOString()}_${name}.png`),
    fullPage: true,
  });
};

export interface BookableCourt {
  courtName: string;
  surface: Surface;
  url: string;
  playingDay: Date;
  hour: number;
}

export const courtToString = ({ courtName, hour }: BookableCourt) =>
  `${courtName} at ${hour}h`;

const getCourtName = (courtId: string) => {
  const n = Number(courtId);
  if (n > 24) {
    return `Clay ${n - 24}`;
  }
  return `Hard ${n}`;
};

const getBookableCourts = withRetry(
  async (
    browser: Browser,
    playingDay: Date,
    surface: Surface,
  ): Promise<BookableCourt[]> => {
    const page = await openPage(browser, getCalendarUrl(playingDay, surface));
    await takeScreenshot(page, `Calendar ${surface}`);
    const urls: string[] = await page.evaluate(() =>
      // @ts-ignore
      $(`table.calendar a:contains(Book)`)
        .get()
        .map((e: any) => e.href),
    );
    await page.goto('about:blank');
    await page.close();
    // reverse so we try to book last courts first
    return reverse(urls).map(url => ({
      url,
      playingDay,
      surface,
      hour: Number((url.match(/time=(\d+)/) || [])[1]),
      courtName: getCourtName((url.match(/item=(\d+)/) || [])[1]),
    }));
  },
);

export const getMatchingCourts = async (
  browser: Browser,
  { playingDay, hours, surface }: BookingPreferences,
) => {
  const surfaces: Surface[] = [surface, surface === 'clay' ? 'hard' : 'clay'];
  const allCourts = flattenDeep(
    await Promise.all(
      surfaces.map(surface => getBookableCourts(browser, playingDay, surface)),
    ),
  );

  return (flattenDeep(
    hours.map(hour =>
      surfaces.map(s =>
        allCourts.filter(court => court.hour === hour && court.surface === s),
      ),
    ),
  ) as any) as BookableCourt[];
};

export const login = withRetry(
  async (prefs: BookingPreferences, browser: Browser) => {
    const page = await openPage(
      browser,
      'http://book.stadeiga.com/courtbooking/home/login.do',
    );
    await page.click('#userid');
    await page.keyboard.type(prefs.email);
    await page.click('#password');
    await page.keyboard.type(prefs.password);
    await page.click('.JegyButton');
    await page.waitForNavigation();
    await wait(1000);

    if (await hasText(page, '.error', 'incorrect')) {
      throw new InvalidCredentialError();
    }
    if (!await hasText(page, 'body', 'Welcome')) {
      throw new StopError('Login failed');
    }
  },
);

// can book 3 days in advance at 20:30
export const getBookingTime = (playingDay: Date) =>
  new Date(
    playingDay.getFullYear(),
    playingDay.getMonth(),
    playingDay.getDate() - 3,
    20,
    30,
    -5, // try to book 5s before
  );

export const hasText = (
  page: Page,
  selector: string,
  text: string,
): Promise<boolean> =>
  page.evaluate(
    (selector, text) =>
      // @ts-ignore
      $(selector)
        .text()
        .includes(text),
    selector,
    text,
  );

export const bookUntilSuccess = withRetry(
  async (browser: Browser, bookUrl: string) => {
    const page = await browser.newPage();
    await page.setViewport({ width: 1200, height: 700 });
    while (true) {
      await retry(100, () => page.goto(bookUrl));

      if (
        await hasText(
          page,
          '.smarteformTabsContent',
          'Your current status does not allow you to reserve',
        )
      ) {
        log('Booking is not open yet. Retrying...');
        continue;
      }
      // Confirm booking
      try {
        await takeScreenshot(page, 'Before confirm');
        await page.click(`input#final`);
        await page.waitForSelector('tr.even');
        await takeScreenshot(page, 'Booking success');
        return;
      } catch {
        await takeScreenshot(page, 'Booking failed');
        throw 'Booking failed';
      }
    }
  },
);

import * as inquirer from 'inquirer';
import { daysOfWeek, dateAdd } from './utils';
import { range } from 'lodash';
// import preferencesJson from './preferences.json';
import * as fs from 'fs';
import * as path from 'path';
import {
  BookingPreferences,
  readPreferences,
  savePreferences,
} from './preferences';

export const askBookingPreferences = async (): Promise<BookingPreferences> => {
  let preferences = readPreferences();

  const responses = (await inquirer.prompt([
    !preferences.email && {
      name: 'email',
      message: 'Account email',
    },
    !preferences.email && {
      name: 'password',
      message: 'Account password',
      type: 'password',
    },
    {
      name: 'playingDay',
      message: 'Play on',
      type: 'list',
      choices: range(1, 7).map(deltaDay => {
        const date = dateAdd(new Date(), { d: deltaDay });
        return {
          name: `${daysOfWeek[date.getDay()]} ${date.getDate()}`,
          value: date,
        };
      }),
      default: 2,
    },
    {
      name: 'hours',
      message: 'at (add alternative hours separed with ,)',
      filter: (s: string) =>
        /[\d,]+/.test(s) && s.split(',').map(s => Number(s)),
      validate: (hours: number[]) =>
        hours && hours.every(n => n >= 7 && n <= 22),
      default: (preferences.hours || []).join(','),
    },
    // TODO
    // {
    //   name: 'is2hours',
    //   message: 'duration',
    //   type: 'list',
    //   choices: [
    //     { name: '1 hour', value: false },
    //     { name: '2 hours', value: true },
    //   ],
    //   default: 0,
    // },
    {
      name: 'surface',
      message: 'prefered surface',
      type: 'list',
      choices: [
        { name: 'hard court', value: 'hard' },
        { name: 'clay court', value: 'clay' },
      ],
      default: preferences.surface,
    },
  ].filter(o => o) as any)) as any;

  const newPreferences = {
    ...preferences,
    ...responses,
  };

  savePreferences(newPreferences);

  return newPreferences;
};

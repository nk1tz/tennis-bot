import * as puppeteer from 'puppeteer';
import * as path from 'path';
import * as fs from 'fs';
import { Question } from 'inquirer';
import {
  getNextBookableDay,
  daysOfWeek,
  waitUntil,
  dateAdd,
  getCalendarUrl,
  wait,
  promiseFilter,
  log,
  login,
  getBookingTime,
  bookUntilSuccess,
  withLoggedBrowser,
  getMatchingCourts,
  courtToString,
  StopError,
  InvalidCredentialError,
  BookableCourt,
} from './utils';
import { range } from 'lodash';
import { askBookingPreferences } from './askBookingPref';
import { readPreferences, savePreferences } from './preferences';

(async () => {
  const prefs = await askBookingPreferences();
  const bookingTime = getBookingTime(prefs.playingDay);

  // TODO: check availabilities right away

  // Will prepare 1 minute before book time
  const prepareBookingTime = dateAdd(bookingTime, { m: -1 });
  if (new Date().getTime() < prepareBookingTime.getTime()) {
    log('Checking for available courts...');
    await withLoggedBrowser(prefs, async browser => {
      let matchingCourts = await getMatchingCourts(browser, prefs);

      if (!matchingCourts.length) {
        log(`WARNING: No court available, you should change your prefs`);
      } else {
        log(`Found ${matchingCourts.length} bookable courts:`);
        matchingCourts.forEach(court => log(courtToString(court)));
      }
    });

    log(`Booking will start ${prepareBookingTime}...`);
    await waitUntil(prepareBookingTime);
  }

  log('Preparing to book');

  await withLoggedBrowser(prefs, async browser => {
    let matchingCourts: BookableCourt[];
    do {
      matchingCourts = await getMatchingCourts(browser, prefs);

      if (!matchingCourts.length) {
        log(`No court available :(`);
        await wait(10000);
      }
    } while (!matchingCourts.length);

    log(`Found ${matchingCourts.length} courts`);

    if (new Date().getTime() < bookingTime.getTime()) {
      log(`Booking ready. Waiting until ${bookingTime}...`);
      await waitUntil(bookingTime);
    }

    log('Booking now!');

    // Try all courts one after the other. Doing it in parrallel seems to make it fail
    for (const court of matchingCourts) {
      try {
        await bookUntilSuccess(browser, court.url);
        log('Booking success!');
        log('Booked:', courtToString(court));
        return;
      } catch {
        log('Failed to book:', courtToString(court));
      }
    }
    log('Booking failed :(');
  });
})().catch(error => {
  if (error instanceof InvalidCredentialError) {
    log('Invalid email or password');

    // remove credentials form saved prefereces so it's asked next time
    const { email, password, ...newPreferences } = readPreferences();
    savePreferences(newPreferences);
  } else if (error instanceof StopError) {
    log('Error:', error.message);
  } else {
    throw error;
  }
});
